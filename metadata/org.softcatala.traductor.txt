Categories:Office
License:GPLv2+
Web Site:https://github.com/Softcatala/TraductorSoftcatalaAndroid
Source Code:https://github.com/Softcatala/TraductorSoftcatalaAndroid
Issue Tracker:https://github.com/Softcatala/TraductorSoftcatalaAndroid/issues

Auto Name:Traductor Softcatalà
Summary:Translate to and from Catalan
Description:
Client for the Softcatalà Catalan on-line [http://www.softcatala.org/traductor translation service].
.

Repo Type:git
Repo:https://github.com/Softcatala/TraductorSoftcatalaAndroid.git

Build:0.7.1,15
    commit=v0.7.1
    target=android-14
    srclibs=1:MobAdMob@3bd7888479e4
    scandelete=libs

Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.7.1
Current Version Code:15

