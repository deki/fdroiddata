Categories:System
License:GPLv3
Web Site:https://github.com/Amphoras/TPT-Helper
Source Code:https://github.com/Amphoras/TPT-Helper/tree/V2.0.3/TPT%20Helper
Issue Tracker:https://github.com/Amphoras/TPT-Helper/issues

Auto Name:TPT Helper
Summary:Tools for ZTE phones
Description:
TPT Helper is designed for the ZTE phones. It helps you to easily repartition the phone and for the ZTE Blade, convert it to the Gen 2 memory layout to let you run the latest custom ROMs. ZTE phones supported so far are the ZTE Blade/Orange San Francisco, ZTE Skate/Orange Monte Carlo and the Orange San Francisco II/T-Mobile Vivacity.

Warning: TPTs can be dangerous and could permanently brick your phone. Only use files designed for your phone, and always follow the in-app instructions. When you flash a TPT, all your data will be wiped. Make a backup first.
.

Repo Type:git
Repo:https://github.com/Amphoras/TPT-Helper.git

Build:2.0.3,24
    commit=e2c80263eef99fd6ab4aa656bfae47a22389493f
    subdir=TPT Helper
    target=android-10

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:2.0.3
Current Version Code:24

