Categories:Games
License:PublicDomain
Web Site:https://gitorious.org/guess-the-number
Source Code:https://gitorious.org/guess-the-number/guess-the-number/
Issue Tracker:https://gitorious.org/guess-the-number/guess-the-number/merge_requests

Auto Name:Guess
Summary:Guess a number
Description:
Simple guessing game. The goal is to guess a random number from 1 to 100 with
8 tries.
.

Repo Type:git
Repo:https://git.gitorious.org/guess-the-number/guess-the-number.git

Build:0.1,1
    commit=7fa1843e2e6d86ca4456517d21418995bcd256ec

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:0.1
Current Version Code:1
