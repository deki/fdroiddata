Disabled:No license
Categories:Navigation
License:Unknown
Web Site:https://github.com/Memetro/memetro-android-app
Source Code:https://github.com/Memetro/memetro-android-app
Issue Tracker:https://github.com/Memetro/memetro-android-app/issues

Auto Name:Memetro
Summary:
Description:
No description available
.

Repo Type:git
Repo:https://github.com/Memetro/memetro-android-app.git

Build:1.0.3,4
    disable=jars, crashlytics, duplicate strings...
    commit=60e4288112494c9

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0.3
Current Version Code:4

