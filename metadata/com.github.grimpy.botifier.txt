Categories:System
License:MIT
Web Site:http://forum.xda-developers.com/showthread.php?t=2421357
Source Code:https://github.com/grimpy/Botifier
Issue Tracker:https://github.com/grimpy/Botifier/issues

Auto Name:Botifier
Summary:Send notifications via Bluetooth
Description:
* Show notifcations on car radio
* Show application as artist
* Show summary as album
* Show full notification text as title
* Play notification via TTS (text to speech)
* Use next / previous track to navigate through notifications
* Use pause / play button to remove notifcation

To test if your media device (car radio) supports AVRCP 1.3 play a mp3 song
which has id3 information set and check if this information is shown on the
media device.
.

Repo Type:git
Repo:https://github.com/grimpy/Botifier.git

Build:1.3.2,14
    commit=1.3.2
    prebuild=sed -i '/key/d' project.properties

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.3.2
Current Version Code:14

