Categories:Internet
License:Apache2
Web Site:https://github.com/DSteve595/Put.io
Source Code:https://github.com/DSteve595/Put.io
Issue Tracker:https://github.com/DSteve595/Put.io/issues

Auto Name:Put.io
Summary:Client for the Put.io online download service
Description:
Manage your Put.io download queue.  
.

Repo Type:git
Repo:https://github.com/DSteve595/Put.io.git

Build:2.0.0-beta1,59
    disable=see maintainer notes
    commit=2.0_beta_1
    subdir=app
    submodules=yes
    gradle=yes
    srclibs=CastCompanion@v1.0

Maintainer Notes:
* no license set
* description
* GoogleCast jars ??
.

Auto Update Mode:None
Update Check Mode:Tags
Current Version:2.0.0-beta6
Current Version Code:65

