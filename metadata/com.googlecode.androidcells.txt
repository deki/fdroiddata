Disabled:now known as org.openbmap
Categories:Internet
License:AGPLv3
Web Site:http://openbmap.org
Source Code:http://sourceforge.net/p/myposition/androidclient
Issue Tracker:http://sourceforge.net/projects/myposition/support

Auto Name:openBmap
Summary:Contribute to coverage maps
Description:
Provides the ability to record cellular and WiFi locations and upload them
to the OpenBMap database. See [http://openbmap.org openbmap.org].
.

Repo Type:git
Repo:git://myposition.git.sourceforge.net/gitroot/myposition/AndroidClient

Build:0.4.96,9
    disable=bc15ce80024
    commit=unknown - see disabled
    target=android-10
    prebuild=mv lib libs

# Jar versions don't tally but may as well try
Build:0.4.999,14
    disable=3ed4e59f
    commit=unknown - see disabled
    target=android-10
    extlibs=openBmap-1.1.tar.gz
    prebuild=tar xvf libs/openBmap-1.1.tar.gz -C src com && \
        mv lib/* libs/ && \
        rm libs/Measure* libs/openBmap*

Auto Update Mode:None
Update Check Mode:Static

