Categories:Reading
License:GPLv3
Web Site:
Source Code:https://github.com/guardianproject/securereader
Issue Tracker:

Auto Name:Courier
Summary:Privacy-aware RSS feed reader
Description:
No description available
.

Repo Type:git
Repo:https://github.com/guardianproject/securereader

Build:0.1.9,13
    disable=native libraries/binaries and jars inside submodules
    commit=v0.1.9
    subdir=app
    submodules=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.1.9
Current Version Code:15

